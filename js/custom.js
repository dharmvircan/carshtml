$(document).ready(function() {
    $('.gentle1').gentleSelect();
    $('.gentle2').gentleSelect();
    $('.gentle3').gentleSelect();
    $('.gentle4').gentleSelect();

    // Home page form starts

    $('#selectBrand').gentleSelect({
        columns: 5,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectModal').gentleSelect({
        columns: 4,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectYear').gentleSelect({
        columns: 2,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectVarient').gentleSelect({
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectState').gentleSelect({
        columns: 2,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#kmDriven').gentleSelect({
        itemWidth: 200,
        openSpeed: 300
    });

    // Footer
    $('#selectBrand_f').gentleSelect({
        columns: 5,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectModal_f').gentleSelect({
        columns: 4,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectYear_f').gentleSelect({
        columns: 2,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectVarient_f').gentleSelect({
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectState_f').gentleSelect({
        columns: 2,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#kmDriven_f').gentleSelect({
        itemWidth: 200,
        openSpeed: 300
    });

    // Autoportal
    $('#selectBrand_auto').gentleSelect({
        columns: 5,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectModal_auto').gentleSelect({
        columns: 4,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectYear_auto').gentleSelect({
        columns: 2,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectVarient_auto').gentleSelect({
        itemWidth: 130,
        openSpeed: 300
    });
    $('#selectState_auto').gentleSelect({
        columns: 2,
        itemWidth: 130,
        openSpeed: 300
    });
    $('#kmDriven_auto').gentleSelect({
        itemWidth: 200,
        openSpeed: 300
    });

    // Home page form starts

    // City Page Appointment form
    $('.gentle5').gentleSelect();
    $('.gentle6').gentleSelect();
    $('.gentle7').gentleSelect();
    $('.gentle8').gentleSelect();
    $('.gentle9').gentleSelect();
    $('.gentle10').gentleSelect();

    // Seo Page City Form
    $('.gentle11').gentleSelect();
    $('.gentle12').gentleSelect();

    //Thank You Page Reschedule
    $('.gentle13').gentleSelect();
    $('.gentle14').gentleSelect();
    $('.gentle15').gentleSelect();
    $('.gentle16').gentleSelect();

});
    //Accordian
    $('.accordionFaq').each(function () {
        var $accordian = $(this);
        $accordian.find('.accordionHead').on('click', function () {
            $(this).parent().find(".accordionHead").removeClass('active inActive');
            $(this).removeClass('active').addClass('inActive');
            $accordian.find('.accordionContent').slideUp();
            if (!$(this).next().is(':visible')) {
                $(this).removeClass('inActive').addClass('active');
                $(this).next().slideDown();
            }
        });
    });

    //Tooltips
    $(".thanksInfo").on('mouseenter touchstart',function () {
        $(this).children().addClass("tooltipsActive");               
    });
    $(".thanksInfo").on('mouseleave touchend',function () {
        $(this).children().removeClass("tooltipsActive");               
    });

    $(".tooltipsCross").click(function(){
        $(".icontooltips").removeClass('tooltipsActive')
    })
 
    //Inspected Crousel
    $("#inspections").flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        slideshow: false,    
        mousewheel: false,
        maxItems: 4,
        rtl: true,
        controlNav: false,
    })
    //Recent Blog Crousel
    $("#cms-blog").flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,    
        slideshow: false,
        mousewheel: false,
        maxItems: 4,
        rtl: true,
        controlNav: false,
    })

    $('ul.gentleselect-dialog ul li').each(function(){
        if($(this).hasClass('selected')) {
            $(this).closest('.select-parent').removeClass("disabled");
        }
    });

    $('.modal').modal({
        show: false,
        keyboard: false,
        backdrop: 'static'
    });